import { useState, useEffect } from 'react';
import ProductsCat from '../components/ProductsCat';

export default function Books() {
  const [books, setBooks] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/stocks`)
      .then(res => res.json())
      .then(data => {
        setBooks(data.map((book, index) => {
          return (
            <ProductsCat key={index} libraryProp={book} />
          );
        }));
      });
  }, []);

  return (
    <>
      {books}
    </>
  );
}