import { useState, useEffect } from 'react';
import { useParams, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import { addToBag, getBag, resetBag, updateBag, removeItem } from '../services/BagService';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';


export default function UserBag() {


  const [bagItems, setBagItems] = useState(getBag());
  const [update, setUpdate] = useState(0);

  const navigate = useNavigate();
 
useEffect(() => {

      setBagItems(getBag())


  }, [update]);



  const handleResetCart = () => {
    resetBag();

   setUpdate(update + 1);
  };

  const handleUpdateCart = (index, e) => {
       
    const quantity = e.target.value;

    const bag = { ...bagItems };

    bag.products[index].quantity = quantity;

    const item = bag.products[index];
  

    updateBag(item.product.productId, quantity );

  setUpdate(update + 1);
  
  };

  const handleRemoveItem = (productId) => {
    removeItem(productId);

     setUpdate(update + 1);

  };

  const handleCheckout = () => {

    fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(

        bagItems

    )
    })
    .then(res => res.json())
    .then(data => {
 
         Swal.fire({
            title: "Great!",
            icon: "success",
            text: "Purchase successfull."
            });
        resetBag();
        setUpdate(update + 1);
        navigate("/profile");
    }).catch (error => {
         Swal.fire({
            title: "Something went wrong!",
            icon: "error",
            text: "Please try again."     
          });
  });
};

  return (
    <>
    
    <div>
      
      <h3>Bag Items:</h3>


      <ul>
        {bagItems && bagItems.products.map((item, index) => (
          <li key={index}> 
            <h5>{item.product.name}</h5>
            <p>Price: {item.product.price}</p>
            <p>Quantity:<input value={item.quantity} type="number" onChange ={e => handleUpdateCart(index, e)} /></p>
            <p>Item total: {Number.parseInt(item.product.price) * Number.parseInt(item.quantity)} </p>
            <button onClick={() => handleRemoveItem(item.product.productId)}>Remove</button>
          </li>
        ))}
      </ul>

      <div>
         <p>Total Amount: {bagItems.products.reduce((total, item)=> total += Number.parseInt(item.product.price) * Number.parseInt(item.quantity), 0)}
          </p>
           <button onClick ={handleCheckout} >Checkout</button>
           <p>

           <Link to="/products">Continue shopping</Link>
           </p>
       </div>
       
    </div>

    </>
  );
}