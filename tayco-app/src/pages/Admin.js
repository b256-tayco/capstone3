import React from 'react';
import UserContext from '../userContext';
import { useState, useEffect, useContext} from 'react';
import { Tab, Tabs, Form, Button, Table, Container, Card} from 'react-bootstrap';
import { getAllUsers, setAsAdmin, allProducts, addProduct } from '../services/AdminService.js'
import ProductsDashboard from '../components/ProDash.js'


export default function Admin() {

const [users, setUsers] = useState([]);


useEffect(() => {
    fetchUsers();
}, [users]);

const fetchUsers = () => {
  getAllUsers()
    .then(data => {
      setUsers(data);
    })
    .catch(error => {
      console.log('Error:', error);
    });
};



const handleSetAdmin = (id) => {
  setAsAdmin(id) 
  fetchUsers()

}

return (
<div>
  <h1> Admin Dashboard  </h1>
    <Tabs defaultActiveKey="users" id="admin-dashboard" className="mb-3">
      <Tab eventKey="users" title="Users">
        <Table striped bordered hover size="sm">
          <thead>
            <tr>
            <th>#</th>
            <th>Username</th>
            <th>Email Address</th>
            <th>Authorization Status</th>
          </tr>
         </thead>
          <tbody>
          {users.map((user, index) => (
            <tr key={index}>
              <td>{index + 1}</td>
              <td>{user.userName}</td>
              <td>{user.email}</td>
              <td>
               {user.isAdmin ? (
                <Button variant="danger" size="sm">Admin </Button>
                   ) : (
                <Button variant="primary" size="sm" active onClick={() => handleSetAdmin(user._id)}>Set as Admin</Button>
                 )}
              </td>
           </tr>
            ))}
          </tbody>   
      </Table>

  </Tab>
      <Tab eventKey="products" title="Products">
        <ProductsDashboard />
      </Tab>
    </Tabs>   
  </div>
);

}
