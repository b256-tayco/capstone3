import Highlight from '../components/Highlight';
import Banner from '../components/Banner';


export default function Home() {

    const data = {
        title: "Welcome, Bookworms!",
        content: "",
        destination: "/"
    }

    return (
        <>
            <Banner data={data}/>
            <Highlight />
        </>
    )
}

