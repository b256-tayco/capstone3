import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2'


export default function Login() {

	const navigate = useNavigate();
	const{user, setUser} = useContext(UserContext);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);


	useEffect (() => {
		if(email !== '' && password !== ''){

			setIsActive(true)

		} else {

			setIsActive(false)

		}
	}, [email,password])


function loginUser(e) {

		e.preventDefault();

	
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)
 

			if (typeof data.access !== "undefined") {

				localStorage.setItem('token', data.access)
			    retrieveUserDetails(data.access)

			    Swal.fire({
			    	title: "Authentication Successful",
			    	icon: "success",
			    	text: "Start bagging!"
			    })
			} else {

				Swal.fire({
			    	title: "Authentication failed",
			    	icon: "error",
			    	text: "Check your login details and try again."
			    })
			}

		})

		setEmail('');
		setPassword('');
	}


const retrieveUserDetails = (token) => {
  fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
    headers: {
      Authorization: `Bearer ${token}`
    }
  })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      });

      if (data.isAdmin) {
        Swal.fire({
          title: "Welcome Admin!",
          text: "You are logged in as an admin.",
          icon: "success"
        }).then(() => {
          navigate('/admin');
        });
      } else {
        navigate('/products');
      }
    });
};


return (

		<>
		<h1>Login</h1>

		<Form onSubmit={e => loginUser(e)}>
	      <Form.Group className="mb-3" controlId="formBasicEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicPassword">
	        <Form.Label>Password</Form.Label>
	        <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)}/>
	      </Form.Group>
	      <Button variant="success" type="submit" id="submitBtn" disabled= {!isActive}>Submit</Button>
	      
	      
	    </Form>
	    </>
)


}

