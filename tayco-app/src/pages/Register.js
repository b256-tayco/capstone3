import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';




export default function Register() {

const { user } = useContext(UserContext);

    const navigate = useNavigate();

	const [userName, setUserName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState(false);

	useEffect(() => {

       
        if((userName !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    },  [userName, email, password1, password2]);



function registerUser(e) {

        e.preventDefault();

         fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        userName: userName,
                        email: email,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {

                    console.log(data);

                    if(data === true){

                      
                        setUserName('');
                        setEmail('');
                        setPassword1('');
                        setPassword2('');

                        Swal.fire({
                            title: 'Registration successful',
                            icon: 'success',
                            text: 'Welcome to Nook Books!'
                        });

                        navigate("/login");

                    } else if (data.message === "User already exists.") {
                        Swal.fire({
                            title: "Registration failed",
                            icon: "error",
                            text: "User with this email already exists. Please provide a different email.",
                         });

                    } else {
                       Swal.fire({
                            title: "Registration failed",
                            icon: "error",
                            text: "Registration failed. Please try again.",
                         });

                    }

            })
}


 return (
		(user.id !== null)?
		<Navigate to="/"/>
		:
		<>
		<h1>Register</h1>

		<Form onSubmit={e => registerUser(e)}>

		  <Form.Group className="mb-3" controlId="formBasicFirstName">
	        <Form.Label>User Name</Form.Label>
	        <Form.Control type="userName" placeholder="Your Alias" value={userName} onChange={e => setUserName(e.target.value)}/>
	      </Form.Group>
	      
	      
	      <Form.Group className="mb-3" controlId="formBasicEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
	      </Form.Group>

	     <Form.Group className="mb-3" controlId="formBasicPassword1">
	        <Form.Label>Password</Form.Label>
	        <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)}/>
	     </Form.Group>

	     <Form.Group className="mb-3" controlId="formBasicPassword2">
	        <Form.Label>Verify Password</Form.Label>
	        <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)}/>
	     </Form.Group>

	      <Button variant={isActive ? "success" : "danger"} type="submit" id="submitBtn" disabled={!isActive}>Submit</Button>
	      

	     </Form>
		 </>
	    
	)

}