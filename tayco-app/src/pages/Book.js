import { useState, useEffect, useContext} from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';
import { addToBag } from '../services/BagService';

export default function Book() {

	const { user } = useContext(UserContext);

	const {productId} = useParams();

	const navigate = useNavigate(); 



	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [genre, setGenre] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(1);

	useEffect(() => {

		console.log(productId);

		fetch(`http://localhost:4000/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			setName(data.name);
			setDescription(data.description);
			setGenre(data.genre);
			setPrice(data.price);
		});
	}, [productId])

 const handleAddToBag = () => {

 	 const product = { productId, name, description, genre, price }


    addToBag(product, quantity);
    Swal.fire({
      title: 'Great!',
      icon: 'success',
      text: 'Purchase successfull.',
    }).then(() => {
      navigate('/bag');
    });
  };

return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-left" >
								<Card.Title className="mb-3">{name}</Card.Title>
								<Card.Subtitle>Description:</Card.Subtitle>
								<Card.Text>{description}</Card.Text>
								<Card.Subtitle>Genre:</Card.Subtitle>
								<Card.Text>{genre}</Card.Text>
								<Card.Subtitle>Price:</Card.Subtitle>
								<Card.Text>PhP {price}</Card.Text>

								<Form.Group className="mb-3">
        						<Form.Label>Qty:</Form.Label>
      						  	<Form.Control type="number" onChange={e => setQuantity(e.target.value)} value={quantity}/>
     						</Form.Group>

								{
									(user.id !== null)?

									<Button variant="primary" onClick={e =>handleAddToBag()}>Add to bag!</Button>
									:
 								  <Link className="btn btn-danger btn-block" to="/login"> Login</Link>
								}
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>			
			)

}