import React from 'react';
import { useEffect, useState } from 'react';
import { Card, Container, Row, Col } from 'react-bootstrap';



const UserProfilePage = ({ user }) => {

const [userDetails, setUserDetails] = useState({});


useEffect(() => {
	retrieveUserDetails();
},[])

const retrieveUserDetails = () => {
  fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`
    }
  })
    .then(res => res.json())
    .then(data => {
      setUserDetails(data);
    });
};


 return (
 	<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
				     <Card style={{ width: 'rem' }}>
				      <Card.Body>
				        <Card.Title>{userDetails.userName}</Card.Title>
				        <Card.Subtitle className="mb-2 text-muted">email: {userDetails.email}</Card.Subtitle>
				      </Card.Body>
				    </Card>
				 </Col>
			</Row>
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
				     <Card style={{ width: 'rem' }}>
				      <Card.Body>
				        <Card.Title>Order History:</Card.Title>
				       {userDetails.orderedProduct?.length && userDetails.orderedProduct.map((order, index) =>
				       	<Card key={index} className= "m-2 p-2">
				       	<p>{new Date(order.purchasedOn).toDateString()}</p>
				       	
				       	<ul>{order.products.map(product =>
				       		
				       		<li key={product._id}>{product.productName}, Qty: {product.quantity}</li>

				       	)}</ul>
				       	<p>Total Amount: {order.totalAmount}</p>
				       	</Card>
				       )}
				      </Card.Body>
				    </Card>
				 </Col>
			</Row>
	</Container>
  );
}

export default UserProfilePage;