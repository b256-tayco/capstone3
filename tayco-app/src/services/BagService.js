

const KEY = "userBag";


export const addToBag = (product, quantity) => {

		const currentBag = JSON.parse(localStorage.getItem(KEY)) || { products: []};

		if(!currentBag.products) currentBag.products=[];


		currentBag.products.push({product, quantity});

		localStorage.setItem(KEY, JSON.stringify(currentBag));

}

export const getBag = () => {

	return JSON.parse(localStorage.getItem(KEY));

}

export const resetBag = () => {

	localStorage.setItem(KEY, JSON.stringify({ products: [] }));
}

export const updateBag = (productId, quantity) => {

	const currentBag = JSON.parse(localStorage.getItem(KEY));

	const found = currentBag.products.find(item => item.product.productId === productId);
	

	if(found) { 
		const index = currentBag.products.indexOf(found);
		found.quantity = quantity;

		currentBag.products[index] = found;

		localStorage.setItem(KEY, JSON.stringify(currentBag));

	} else {
		console.log("Product not found")
	}
}

export const removeItem = (productId) => {

	const currentBag = JSON.parse(localStorage.getItem(KEY));

	const products = currentBag.products.filter(book => book.product.productId !== productId );

	currentBag.products = products;

	localStorage.setItem(KEY, JSON.stringify(currentBag));

}

export const userOrders = (orders) =>{

}