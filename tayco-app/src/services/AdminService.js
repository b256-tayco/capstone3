
const getToken = () => {
  return localStorage.getItem('token');
}

export const getAllUsers = () => {
    return fetch(`${process.env.REACT_APP_API_URL}/users/allusers`, {
      headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${getToken()}`
    }
    })
    .then(res => res.json())
    .then(data => {
        return data
        //console.log(data)
    })
    .catch(error => {
   
      throw error;
    });
};

export const allProducts = (products) => {

	return fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
      headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${getToken()}`
    }
    })
	.then(res => res.json())
	.then(data => {
		return data;
	});
};

export const addProduct = (product) => {
	return fetch(`${process.env.REACT_APP_API_URL}/products/add`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${getToken()}`
    },
    body: JSON.stringify(product)
  })
    .then(res => res.json())
    .then(data => {

      return data;
    })
    .catch(error => {
   
      return error;
    });
};

export const updateProduct = (productId, product) => {
  return fetch(`${process.env.REACT_APP_API_URL}/products/update/${productId}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${getToken()}`
    },
    body: JSON.stringify(product)
  })
    .then(res => res.json())
    .then(data => {
    
      return data;
    })
    .catch(error => {
     
      return error;
    });
};

export const archiveProduct = (productId) => {
  return fetch(`${process.env.REACT_APP_API_URL}/products/archive/${productId}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('accessToken') // Include the authorization header if required
    }
  })
    .then(res => res.json())
    .then(data => {
      
      return data;
    })
    .catch(error => {
    
      return error;
    });
};

export const restockProduct = (productId) => {
  return fetch(`${process.env.REACT_APP_API_URL}/products/restock/${productId}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('accessToken') // Include the authorization header if required
    }
  })
    .then(res => res.json())
    .then(data => {
     
      return data;
    })
    .catch(error => {
     
      return error;
    });
};

export const getProfile = (userId) => {
  return fetch(`${process.env.REACT_APP_API_URL}/profile/${userId}`)
    .then(res => res.json())
    .then(data => {
     
      data.password = "";
      return data;
    })
    .catch(error => {
      return error;
    });
};

export const setAsAdmin = (userId) => {
  return fetch(`${process.env.REACT_APP_API_URL}/users/setAdmin/${userId}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${getToken()}` 
    }
  })
    .then(res => res.json())
    .then(data => {
       alert('User set as admin successfully');
      return data;
    })
    .catch(error => {
      
      return error;
    });
};

export const getAllOrders = (orders) => {
  return fetch(`${process.env.REACT_APP_API_URL}/orders/all`)
    .then(res => res.json())
    .then(data => {
      
      return data;

    })
    .catch(error => {
      
      return error;
    });
};

export const getUserOrders = (userId) => {
  return fetch(`${process.env.REACT_APP_API_URL}/orders/user/${userId}`, {
     method: 'GET',
      headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${getToken()}`
    },
    body: JSON.stringify(userId)
  })
    .then(res => res.json())
    .then(data => {
     
      return data;
    })
    .catch(error => {
     
      return error;
    });
};

