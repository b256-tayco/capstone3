import './App.css';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import{ BrowserRouter as Router, Route, Routes, Navigate } from 'react-router-dom' ;
import { UserProvider } from './userContext';
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Admin from './pages/Admin';
import Products from './pages/Products';
import Book from './pages/Book';
import UserBag from './pages/UserBag'
import Logout from './pages/Logout';
import UserProfilePage from './pages/Profile';
import jwt from 'jwt-decode'


function App() {

    const [user,setUser] = useState({
      id: null,
      isAdmin: null
  });

    const unsetUser = () => {
    localStorage.clear();
  }


  useEffect(() => {
    console.log(user)
    console.log(localStorage)

    const token = localStorage.getItem('token')

    if(token){
     const { id, isAdmin } = jwt(token)
     setUser({ id, isAdmin });
    }
  },[]);


  return (
   
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
    <AppNavBar /> 
    <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/products" element={<Products />} />
            <Route path="/book/:productId" element={<Book />} />
            <Route path="/bag" element={(user.isAdmin ?  <Navigate to="/" /> : <UserBag /> )} />
            <Route path="/profile" element={(user.isAdmin ?  <Navigate to="/" /> : <UserProfilePage user={user}/> )} />
            <Route path="/admin" element={(user.isAdmin ? <Admin /> : <Navigate to="/" />)} />
            <Route path="/logout" element={<Logout />} />

          </Routes>
    </Container>
    </Router>
    </UserProvider>


  );
}

export default App;
