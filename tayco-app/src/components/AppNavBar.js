import { useContext } from 'react';
import { Container, Navbar, Nav} from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../userContext';
import  nblogo from '../pics/nblogo.png';

export default function AppNavBar() {

 
	const { user } = useContext(UserContext);

	return (
		<Navbar className="custom-navbar"  bg="light" expand="lg">
  		<Container>
  			  <Navbar.Brand as={Link} to="/">
  			  	<img
              src={nblogo}
              width="200"
              height="30"
              className="d-inline-block align-top"
              alt="NOOK BOOKS"
            />
  			  </Navbar.Brand>
  			  <Navbar.Toggle aria-controls="basic-navbar-nav" />
   				 <Navbar.Collapse id="basic-navbar-nav">
    			 <Nav className="me-auto">
      				<Nav.Link as={NavLink} to="/">Home</Nav.Link>
      				<Nav.Link as={NavLink} to="/products">Shop</Nav.Link>
      		
      				{
      				 	(user.id !== null) ?
      				 	<>
      				 	<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>

                        {!user.isAdmin && (<Nav.Link as={NavLink} to="/profile">Profile</Nav.Link>)}
      				 	</>
      				    :  
      				 	<>
      				 	<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
      				    <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
      				    </>
      				 }

      				 {user.isAdmin && ( <Nav.Link as={NavLink} to="/admin">Admin Dashboard</Nav.Link> )}
      			 </Nav>
   				 </Navbar.Collapse>
 		 </Container>
   		 </Navbar>

		)
}
