import React from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import { useState, useEffect } from 'react';

export default function InputModal(props) {

const { bookInfo, handleAddNewBook, handleUpdate } = props;

const [name, setName] =useState('');
const [description, setDescription] =useState('');
const [genre, setGenre] = useState('');
const [price, setPrice] = useState('');
const [inStock, setInStock] = useState(false);

useEffect(() => {
	setName(bookInfo.name);
	setDescription(bookInfo.description);
	setGenre(bookInfo.genre);
	setPrice(bookInfo.price);
	setInStock(bookInfo.inStock);

}, [bookInfo]);

const addButton = () => {
  const product = {
    name,
    description,
    genre,
    price,
    inStock
  };
  handleAddNewBook(product);
}

const updatebtn = () => {
   const product = {
    name,
    description,
    genre,
    price,
    inStock
  };
  handleUpdate(bookInfo._id, product);
}


return (

	 
      <Modal show={props.show} onHide={props.onHide}>
        <Modal.Header closeButton>
          <Modal.Title>Book Information</Modal.Title>
        </Modal.Header>

        <Modal.Body>
            <Form.Control controlid="productName" type="text" placeholder="Book title and Author" value={name} onChange={e=>setName(e.target.value)}/>
          <br />
            <Form.Group className="mb-3" controlid="productDescription">
             <Form.Label>Description:</Form.Label>
             <Form.Control as="textarea" rows={3} value={description} onChange={e=>setDescription(e.target.value)}/>
            </Form.Group>
          <br />
            <Form.Control controid="productGenre" type="text" placeholder="Genre" value={genre} onChange={e=>setGenre(e.target.value)}/>
          <br />  
          <Form.Control controid="productPrice" type="text" placeholder="Price" value={price} onChange={e=>setPrice(e.target.value)}/>
          <br />
            <Form.Group className="mb-3" controlid="inStock">
                <Form.Check type="checkbox" label="Available" checked={inStock} onChange={e=>setInStock(!inStock)}/>
            </Form.Group>
            
        </Modal.Body>

        <Modal.Footer>
              {Object.keys(bookInfo).length ?
                <Button variant="primary" onClick={updatebtn}>Update Book </Button>
                :
                <Button variant="primary" onClick={addButton}>Add Book</Button>
            }

        </Modal.Footer>
      </Modal>



	);
}