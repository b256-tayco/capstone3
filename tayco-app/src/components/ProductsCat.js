import {Card, Button, Row, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom';



export default function ProductCat({libraryProp}) {

const {name, description, genre, price, _id} = libraryProp;


return(
		<Row xs={1} md={2} className="g-2">
     	 {Array.from({ length: 1 }).map((_, idx) => (
        <Col className = "grid" style={{ padding: '5px', margin: '5px' }}>
				<Card>
			      	<Card.Body>
			       			 <Card.Title>{name}</Card.Title>
			         			  <Card.Text>{description}</Card.Text>
			         			  <Card.Text>{genre}</Card.Text>
			         			  <Card.Subtitle>Price:{price}</Card.Subtitle>
			         			  <br/>
			       				 <Button variant="primary" as={Link} to={`/book/${_id}`}>Details</Button>
			     	</Card.Body>
			    </Card>	
			</Col>
      ))}
    </Row>	
	);

}