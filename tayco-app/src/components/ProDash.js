import React from 'react';
import UserContext from '../userContext';
import { useState, useEffect, useContext} from 'react';
import { Tab, Tabs, Form, Button, Table, Container, Card, Badge} from 'react-bootstrap';
import { allProducts, addProduct, updateProduct } from '../services/AdminService.js'
import InputModal from './Modal.js'
import Swal from 'sweetalert2';

export default function ProductsDashboard(prop) {

const [products, setProducts] = useState([]);
const [showModal, setShowModal] = useState(false);
const [selected, setSelected] =useState({});

useEffect(() => {
    fetchProducts();
}, [products]);

const fetchProducts =() => {
  allProducts()
  .then(data => {
    setProducts(data);
  })
  .catch(error => {
      console.log('Error:', error);
  });
};

const addBookInfo = () => {
	setShowModal(true);
	setSelected({});
	

};

const updateBook = (bookInfo) => {
	setShowModal(true);
	setSelected(bookInfo);

console.log(selected)
};

const handleHide = () => {
	setSelected({});
  setShowModal(false);
}

const handleAddNewBook = (bookInfo) => {
	addProduct(bookInfo);
	handleHide();

	Swal.fire({
            title: "Success!",
            icon: "success",
            text: "You have added a new book to our inventory!"
            });
	fetchProducts()
}

const handleUpdate = (id, bookInfo) => {
	updateProduct(id, bookInfo);
	handleHide();
	Swal.fire({
            title: "Success!",
            icon: "success",
            text: "You have updated a book information!"
            });
	fetchProducts()
}


return (

<>
	<div>
        <Container>         
             <Button variant="primary" onClick={addBookInfo} > Add Book</Button>   
        </Container>
        <br/>
        <Container>

          <h2> Update Shop </h2>

            <Table striped bordered hover size="sm">
	          <thead>
	            <tr>
	            <th>#</th>
	            <th>Title and Author</th>
	            <th>Description</th>
	            <th>Genre</th>
	            <th>Price</th>
	            <th>Availability</th> 
	            <th>Action</th>
	          </tr>
	         </thead>
		          <tbody>
		          {products.map((product, index) => (
		            <tr key={index}>
		              <td>{index + 1}</td>

		              <td>{product.name}</td>
		              <td>{product.description}</td>
		              <td>{product.genre}</td>
		              <td>{product.price}</td>
		              <td>
		               {product.inStock ? (
		                <Badge bg="primary">Available </Badge>
		                   ) : (
		                <Badge bg="danger">Out of Stock</Badge>
		                 )}
		              </td>
		                <td> <Button variant="danger" size="sm" onClick={() => updateBook(product)}>Update </Button> </td>
		           </tr>
		            ))}
		          </tbody>   
      		</Table>     
     	</Container>
	</div>

	<InputModal show={showModal} onHide={handleHide} bookInfo={selected} handleAddNewBook={handleAddNewBook} handleUpdate={handleUpdate} />
 </>




)

};